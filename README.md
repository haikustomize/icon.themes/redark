# Redark
---
Icons with white outline that stand out a bit more than original when setting a dark theme in the Appearance preferences

These icons are designed with dark Appearance themes in mind. That is, a dark background. They are designed with a white outline to make them "pop" on black or dark backgrounds, and light comming from below, from the white lamp at the left bottom corner of the icon, well, sort of.

The style of these icons is based on default Haiku Icons' style.

The files included here are Icon-O-Matic files.

Not all icons are directly usable on Haiku as it does not support icon themes yet. In order to use these icons you can do:

1. drag and drop the proper icon from this list to the correspondant filetype in the FileTypes preferences Icon well
2. add the icon as attribute to a folder

Software icons can't be changed as they are part of the resource files.

![alt text][logo]

[logo]:redark-icons.png


